import React from 'react';

class ConferenceForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            max_presentations: '',
            max_attendees: '',
            locations: []
          };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleStart = this.handleStart.bind(this);
        this.handleEnd = this.handleEnd.bind(this);
        this.handleDescription = this.handleDescription.bind(this);
        this.handleMaxPresentation = this.handleMaxPresentation.bind(this);
        this.handleMaxAttendee = this.handleMaxAttendee.bind(this);
        this.handleLocation = this.handleLocation.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        // data.max_presentation = data.maxPresentations;
        // data.max_attendees = data.maxAttendees;
        // delete data.maxPresentations;
        // delete data.maxAttendees;
        delete data.locations;
        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            const cleared = {
                name: '',
                starts: '',
                ends: '',
                description: '',
                max_presentations: '',
                max_attendees: '',
                location: '',
            };
              this.setState(cleared);
            }
        }
    

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
    }

    handleStart(event) {
        const value = event.target.value;
        this.setState({starts: value})
    }

    handleEnd(event) {
        const value = event.target.value;
        this.setState({ends: value})
    }

    handleDescription(event) {
        const value = event.target.value;
        this.setState({description: value})
    }

    handleMaxPresentation(event) {
        const value = event.target.value;
        this.setState({max_presentations: value})
    }

    handleMaxAttendee(event) {
        const value = event.target.value;
        this.setState({max_attendees: value})
    }

    handleLocation(event) {
        const value = event.target.value;
        this.setState({location: value})
    }


    async componentDidMount() {
        const url = 'http://localhost:8000/api/locations/';
    
        const response = await fetch(url);
    
        if (response.ok) {
          const data = await response.json();
          this.setState({locations: data.locations});
            console.log(data)
        }
      }

  render() {
    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new conference</h1>
              <form onSubmit={this.handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                  <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                  <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handleStart} value={this.state.starts} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" />
                  <label htmlFor="starts">Starts</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handleEnd} value={this.state.ends} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" />
                  <label htmlFor="ends">Ends</label>
                </div>
                <div className="mb-3">
                  <label htmlFor="exampleFormControlTextarea1" className="form-label">Description</label>
                  <textarea onChange={this.handleDescription} value={this.state.description} className="form-control" name="description" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handleMaxPresentation} value={this.state.max_presentations} placeholder="Max presentations" required type="text" name="max_presentations" id="max_presentations" className="form-control" />
                  <label htmlFor="max_presentations">Maximum presentations</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handleMaxAttendee} value={this.state.max_attendees} placeholder="Max attendees" required type="text" name="max_attendees" id="max_attendees" className="form-control" />
                  <label htmlFor="max_attendees">Maximum Attendees</label>
                </div>
                <div className="mb-3">
                  <select onChange={this.handleLocation} value={this.state.location} required id="location" name="location" className="form-select">
                    <option value="">Choose a location</option>
                    {this.state.locations.map(location => {
                        // console.log(location)
                        return (
                        <option key={location.href} value={location.id}>
                            {location.name}
                            </option>
                        );
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ConferenceForm;