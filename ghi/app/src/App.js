import Nav from './Nav';
import LocationForm from './LocationForm';
import AttendeesList from './AttendeesList';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage.js';
import React from 'react';
import { BrowserRouter, Routes, Route } from "react-router-dom";


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
          {/* <div className="container"> */}
            <Routes>
              <Route path="locations/new" element={<LocationForm />} />
              <Route path="/conferences/new" element={ <ConferenceForm />} />
              <Route path="/attendees/new" element={ <AttendConferenceForm /> } />
              <Route path="/attendees" element={<AttendeesList attendees={props.attendees} />} />
              <Route path="/presentations/new" element={<PresentationForm />} />
              <Route index element={<MainPage />} />
            </Routes>
        {/* </div> */}
    </BrowserRouter>
      );
}
export default App;